import re


def _7_1():
    regex = re.compile(
        r"^(\w+\s\w+)\s\w+\scontain\s(\d?)\s(\w+\s\w+)\s\w+(,\s(\d)\s(\w+\s\w+)\s\w+)?(,\s(\d)\s(\w+\s\w+)\s\w+)?(,\s(\d)\s(\w+\s\w+)\s\w+)?.$",
        re.IGNORECASE
    )
    luggage_dict = {}
    checked_bag_types = set([])
    with open("d07/d07.input", "r") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln and "no other bags" not in ln:
                match = re.match(regex, ln)
                container, content_1_amount, content_1 = (match.group(1), match.group(2), match.group(3))
                try:
                    content_2_amount, content_2 = (match.group(5), match.group(6))
                except IndexError:
                    content_2_amount, content_2 = (None, None)
                try:
                    content_3_amount, content_3 = (match.group(8), match.group(9))
                except IndexError:
                    content_3_amount, content_3 = (None, None)
                try:
                    content_4_amount, content_4 = (match.group(11), match.group(12))
                except IndexError:
                    content_4_amount, content_4 = (None, None)

                luggage_dict.update({
                    container: {
                        content_1: content_1_amount,
                        content_2: content_2_amount,
                        content_3: content_3_amount,
                        content_4: content_4_amount,
                    }
                })
    for sub_dict in luggage_dict.values():
        try:
            sub_dict.pop(None)
        except KeyError:
            pass

    def rec_determine_valid_bags(
            target_bag_name="shiny gold",
            cbt=checked_bag_types,
            data_set=luggage_dict,
    ):
        for outer_key, outer_val in data_set.items():
            for inner_key, inner_val in outer_val.items():
                if inner_key == target_bag_name and outer_key not in cbt:
                    cbt.add(outer_key)
                    cbt.union(rec_determine_valid_bags(outer_key, cbt))
        return cbt

    checked_bag_types = rec_determine_valid_bags()
    print(len(checked_bag_types), checked_bag_types)


def _7_2():
    regex = re.compile(
        r"^(\w+\s\w+)\s\w+\scontain\s(\d?)\s(\w+\s\w+)\s\w+(,\s(\d)\s(\w+\s\w+)\s\w+)?(,\s(\d)\s(\w+\s\w+)\s\w+)?(,\s(\d)\s(\w+\s\w+)\s\w+)?.$",
        re.IGNORECASE
    )
    luggage_dict = {}
    with open("d07/d07.input", "r") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln and "no other bags" not in ln:
                match = re.match(regex, ln)
                container, content_1_amount, content_1 = (match.group(1), match.group(2), match.group(3))
                try:
                    content_2_amount, content_2 = (match.group(5), match.group(6))
                except IndexError:
                    content_2_amount, content_2 = (None, None)
                try:
                    content_3_amount, content_3 = (match.group(8), match.group(9))
                except IndexError:
                    content_3_amount, content_3 = (None, None)
                try:
                    content_4_amount, content_4 = (match.group(11), match.group(12))
                except IndexError:
                    content_4_amount, content_4 = (None, None)

                luggage_dict.update({
                    container: {
                        content_1: content_1_amount,
                        content_2: content_2_amount,
                        content_3: content_3_amount,
                        content_4: content_4_amount,
                    }
                })
    for sub_dict in luggage_dict.values():
        try:
            sub_dict.pop(None)
        except KeyError:
            pass

    def rec_determine_bag_amount(target_bag="shiny gold", data_set=luggage_dict):
        result = 0
        try:
            bag_contents = data_set[target_bag]
        except KeyError:
            return 0
        for key, val in bag_contents.items():
            result += int(val) * (1 + rec_determine_bag_amount(key))
        return result

    bag_amount = rec_determine_bag_amount()
    print(bag_amount)
