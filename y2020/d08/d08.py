import re


class ProcessorOp(object):
    op_str = ""
    op_int = 0
    executed = False

    def __init__(self, op_str, op_int, executed):
        self.op_str = op_str
        self.op_int = int(op_int)
        self.executed = executed


def _8_1():

    regex = re.compile(r"^(\w+)\s([+-]\d+)$", re.IGNORECASE)
    operations_list = []
    with open("d08/d08.input", "r") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln and ln is not "-":
                operations_list.append(ProcessorOp(re.match(regex, ln).group(1), re.match(regex, ln).group(2), False))
            else:
                accumulator = 0
                ln_index = 0
                while not operations_list[ln_index].executed:
                    op = operations_list[ln_index]
                    if op.op_str == "acc":
                        accumulator += op.op_int
                        ln_index += 1
                    elif op.op_str == "jmp":
                        ln_index += op.op_int
                    else:
                        ln_index += 1
                    op.executed = True
                print(accumulator)


def _8_2():

    regex = re.compile(r"^(\w+)\s([+-]\d+)$", re.IGNORECASE)
    operations_list = []
    with open("d08/d08.input", "r") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln and ln is not "-":
                operations_list.append(ProcessorOp(re.match(regex, ln).group(1), re.match(regex, ln).group(2), False))
            else:

                def run_boot_code(op_list=operations_list):
                    for op in op_list:
                        op.executed = False

                    accumulator = 0
                    ln_index = 0
                    while not op_list[ln_index].executed:
                        op = op_list[ln_index]
                        if op.op_str == "acc":
                            accumulator += op.op_int
                            ln_index += 1
                        elif op.op_str == "jmp":
                            ln_index += op.op_int
                        else:
                            ln_index += 1
                        op.executed = True
                        if ln_index == len(op_list):
                            break
                    return accumulator, ln_index

                for index, operation in enumerate(operations_list):
                    if operation.op_str == "nop":
                        operation.op_str = "jmp"
                        accumulator, ln_index = run_boot_code()
                        operation.op_str = "nop"

                    elif operation.op_str == "jmp":
                        operation.op_str = "nop"
                        accumulator, ln_index = run_boot_code()
                        operation.op_str = "jmp"
                    else:
                        continue

                    if ln_index == len(operations_list):
                        print(accumulator)
                        break
