

def _6_1():
    answers_set = set([])
    answers_sum = 0
    with open("d06/d06.input", "r") as f:
        for ln_index, ln in enumerate(f):
            ln = ln.rstrip()
            if ln and ln != "-":
                for char in ln:
                    answers_set.add(char)
            elif ln == "-" or not ln:
                answers_sum += len(answers_set)
                answers_set = set([])
    print(answers_sum)


def _6_2():
    answers_sum = 0
    group_answers_str = ""
    group_members_num = 0
    with open("d06/d06.input", "r") as f:
        for ln_index, ln in enumerate(f):
            ln = ln.rstrip()
            if ln and ln != "-":
                group_answers_str += ln
                group_members_num += 1
            elif ln == "-" or not ln:
                for letter in "abcdefghijklmnopqrstuvwxyz":
                    if group_answers_str.count(letter) == group_members_num:
                        answers_sum += 1
                group_answers_str = ""
                group_members_num = 0
    print(answers_sum)
