import re


def _3_0():
    with open("d03/d03.input", "r") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln:
                print(1000*ln)


def _3_1():
    with open("d03/d03.input_extended", "r") as f:
        char_pos = 0
        trees = 0
        for ln_index, ln in enumerate(f):
            ln = ln.rstrip()
            if ln:
                try:
                    if ln[char_pos] == "#":
                        trees += 1
                except IndexError:
                    break
                char_pos += 3
        print(trees)


def _3_2():
    with open("d03/d03.input_extended", "r") as f:
        char_pos_1_1, char_pos_3_1, char_pos_5_1, char_pos_7_1, char_pos_1_2,\
            trees_1_1, trees_3_1, trees_5_1, trees_7_1, trees_1_2 = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        for ln_index, ln in enumerate(f):
            ln = ln.rstrip()
            if ln:
                try:
                    if ln[char_pos_1_1] == "#":
                        trees_1_1 += 1
                    if ln[char_pos_3_1] == "#":
                        trees_3_1 += 1
                    if ln[char_pos_5_1] == "#":
                        trees_5_1 += 1
                    if ln[char_pos_7_1] == "#":
                        trees_7_1 += 1
                    if ln_index % 2 == 0 and ln[char_pos_1_2] == "#":
                        trees_1_2 += 1
                except IndexError:
                    print(ln_index)
                    break
                char_pos_1_1 += 1
                char_pos_3_1 += 3
                char_pos_5_1 += 5
                char_pos_7_1 += 7
                if ln_index % 2 == 0:
                    char_pos_1_2 += 1
        print(trees_1_1, trees_3_1, trees_5_1, trees_7_1, trees_1_2)
        print(trees_1_1 * trees_3_1 * trees_5_1 * trees_7_1 * trees_1_2)
