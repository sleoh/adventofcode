import re


class PositionManipulator(object):
    p_north_south = 0
    p_east_west = 0

    def __init__(self, p_north_south: int, p_east_west: int):
        self.p_north_south = p_north_south
        self.p_east_west = p_east_west

    def north(self, increment):
        self.p_north_south -= increment

    def east(self, increment):
        self.p_east_west += increment

    def south(self, increment):
        self.p_north_south += increment

    def west(self, increment):
        self.p_east_west -= increment

    def manhattan_distance(self):
        return abs(self.p_north_south) + abs(self.p_east_west)


class Waypoint(PositionManipulator):

    def rotate(self, direction, degrees):
        if degrees == 180:
            self.p_north_south *= -1
            self.p_east_west *= -1
        elif (direction == "R" and degrees == 90) or (direction == "L" and degrees == 270):
            new_p_ns = self.p_east_west
            new_p_ew = self.p_north_south * -1
            self.p_north_south = new_p_ns
            self.p_east_west = new_p_ew
        elif (direction == "L" and degrees == 90) or (direction == "R" and degrees == 270):
            new_p_ns = self.p_east_west * -1
            new_p_ew = self.p_north_south
            self.p_north_south = new_p_ns
            self.p_east_west = new_p_ew


class Ship(PositionManipulator):
    facing = 0
    waypoint = None

    def __init__(self, facing: int, p_north_south: int, p_east_west: int, waypoint: Waypoint):
        super(Ship, self).__init__(p_north_south=p_north_south, p_east_west=p_east_west)
        self.facing = facing
        self.waypoint = waypoint

    def forward(self, increment):
        if self.facing == 0:
            self.north(increment=increment)
        elif self.facing == 90:
            self.east(increment=increment)
        elif self.facing == 180:
            self.south(increment=increment)
        elif self.facing == 270:
            self.west(increment=increment)

    def to_waypoint(self, increment):
        if self.waypoint:
            self.p_north_south += self.waypoint.p_north_south * increment
            self.p_east_west += self.waypoint.p_east_west * increment

    def rotate(self, direction, degrees):
        if direction == "L":
            degrees = 360 - degrees
        self.facing = (self.facing + degrees) % 360


def _12_1():
    regex = re.compile(r"^([A-Z])(\d+)$", re.IGNORECASE)
    with open("d12/d12.input", "r") as f:
        ferry = Ship(facing=90, p_north_south=0, p_east_west=0, waypoint=None)
        for ln in f:
            ln = ln.rstrip()
            if ln and ln is not "-":
                action, value = (re.match(regex, ln).group(1), int(re.match(regex, ln).group(2)))
                if action in ["N", "E", "S", "W"]:
                    if action == "N":
                        ferry.north(value)
                    elif action == "E":
                        ferry.east(value)
                    elif action == "S":
                        ferry.south(value)
                    elif action == "W":
                        ferry.west(value)
                elif action == "F":
                    ferry.forward(value)
                elif action in ["L", "R"]:
                    ferry.rotate(action, value)
            else:
                print(ferry.manhattan_distance())


def _12_2():
    regex = re.compile(r"^([A-Z])(\d+)$", re.IGNORECASE)
    with open("d12/d12.input", "r") as f:
        waypoint = Waypoint(p_north_south=-1, p_east_west=10)
        ferry = Ship(facing=90, p_north_south=0, p_east_west=0, waypoint=waypoint)
        for ln in f:
            ln = ln.rstrip()
            if ln and ln is not "-":
                action, value = (re.match(regex, ln).group(1), int(re.match(regex, ln).group(2)))
                if action in ["N", "E", "S", "W"]:
                    if action == "N":
                        waypoint.north(value)
                    elif action == "E":
                        waypoint.east(value)
                    elif action == "S":
                        waypoint.south(value)
                    elif action == "W":
                        waypoint.west(value)
                elif action == "F":
                    ferry.to_waypoint(value)
                elif action in ["L", "R"]:
                    waypoint.rotate(action, value)
            else:
                print(ferry.manhattan_distance())
